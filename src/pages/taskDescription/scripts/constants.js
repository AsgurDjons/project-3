export const constants = {
  columns: document.querySelectorAll(".task__column"),
  addButtons: document.querySelectorAll(".item__btn"),
  submit: document.querySelector('input[type="submit"]'),
  commentInput: document.querySelector(".comment__input"),
  userName: document.querySelector(".user__name"),
  userRole: document.querySelector(".user__role"),
  commentHistory: document.querySelector(".comment__history"),
  taskDescription: document.querySelector(
    ".task__desctiption.task__desctiption__card"
  ),
  projectLinks: document.querySelectorAll(".projects__icon"),
  header: document.querySelector('h1')
};
