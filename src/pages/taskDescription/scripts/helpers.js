import { constants } from "./constants";

const { commentHistory, taskDescription } = constants;

export function createComment(comment) {
  return `<div class="commet comment__block">
  <div class="commet__icon">
    <img src="img/userpic-big.jpg" alt="">
  </div>
  <div class="commet__text__wrapp">
    <p class="commet__name">${comment.name},<span class="commet__role">${comment.role}</span></p>

    <p class="commet__text">${comment.text}</p>

    <p class="commet__date">${comment.date}</p>
  </div>
</div>`;
}

export function renderComment(comment) {
  const commentHTML = createComment(comment);

  commentHistory.insertAdjacentHTML("beforeend", commentHTML);
}

export function createTaskAtDescription(task, id) {
  return `<a href="task-description.html?id=${
    task.id
  }" class="task task__block ${task.finished ? "task--finished" : ""} ${
    task.id === +id ? "task--active" : ""
  }">
  <span class="checkbox">
    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">
        <path id="Path" d="M11.561.44a1.5,1.5,0,0,0-2.124,0L4.5,5.379,2.56,3.441A1.5,1.5,0,0,0,.439,5.562l3,3a1.5,1.5,0,0,0,2.121,0l6-6A1.5,1.5,0,0,0,11.561.44Z" fill="#b8b8b8" />
    </svg>
    <input type="checkbox">
    
  </span>
  <div>
    <p class="task__description">${task.header}</p>
    <div class="flex">
      <img src="img/userpic.jpg" class="task__executor" alt="">
      <p class="task__label ${task.tag}">${task.tag.toUpperCase()}</p>
    </div>
  </div>					
</a>`;
}

export function renderTaskAtDescription(task, id) {
  const taskHTML = createTaskAtDescription(task, id);

  const targetHeader = Array.from(
    document.querySelectorAll(".item__title")
  ).find((el) => el.textContent === task.stage);

  if (!targetHeader) {
    return;
  }

  const targetColumn = targetHeader.parentNode.parentNode;

  targetColumn.insertAdjacentHTML("beforeend", taskHTML);
}

export function createTaskDescription(task) {
  return `<div class="task__desctiption__header">
<div class="item__header task ${task.finished ? "task--finished" : ""}">
  <div>
    <h2 class="item__title">${task.header}</h2>
    <p class="author__list">Added by Kristin A. yesterday at 12:41pm</p>
  </div>

  <div class="checkbox checkbox--big">
    <input type="checkbox" checked="">
    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">
        <path id="Path" d="M11.561.44a1.5,1.5,0,0,0-2.124,0L4.5,5.379,2.56,3.441A1.5,1.5,0,0,0,.439,5.562l3,3a1.5,1.5,0,0,0,2.121,0l6-6A1.5,1.5,0,0,0,11.561.44Z" fill="#b8b8b8" />

    </svg>
  </div>
</div>

<div class="info__block flex">
  <div class="execuror__block">
    <p class="legend">Исполнитель</p>

    <div class="executor flex">
      <img src="img/userpic-big.jpg" alt="" class="executor__img">
      <p class="executor__name">${task.executor}</p>
    </div>
  </div>
  <div class="dueon__block">
    <p class="legend legend--bold">DUE ON</p>
    <p class="dueon__date legend">${task.deadline}</p>
  </div>
  <div class="tag__block">
    <p class="legend legend--bold">TAG</p>
    <p class="task__label ${task.tag}">${task.tag.toUpperCase()}</p>
  </div>
</div>

<div class="task__desctiption__textarea">
  <p>${task.description}</p>
</div>
</div>`;
}

export function renderTaskDescription(task) {
  const taskHTML = createTaskDescription(task);

  taskDescription.insertAdjacentHTML("afterbegin", taskHTML);
}
