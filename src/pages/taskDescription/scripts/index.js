import "../../../css/style.scss";
import { constants } from "./constants";
import {
  renderComment,
  renderTaskAtDescription,
  renderTaskDescription,
} from "./helpers";
import {
  initTasks,
  getFromStorage,
  setToStorage,
  updateArrayItem,
} from "../../../api";
import {addTeamCommands} from '../../team/scripts/index';
const { submit, commentInput, userName, userRole, projectLinks, header } =
  constants;

initTasks();
addTeamCommands();
function getIdFromUrl() {
  const url_string = window.location.href;
  const url = new URL(url_string);
  const id = url.searchParams.get("id");

  return id;
}

function getDateString() {
  const timestamp = new Date();

  function getDoubleDigits(digit) {
    return digit > 9 ? digit : "0" + digit;
  }

  return `${getDoubleDigits(timestamp.getDate())}.${getDoubleDigits(
    timestamp.getMonth()
  )} ${getDoubleDigits(timestamp.getHours())}:${getDoubleDigits(
    timestamp.getMinutes()
  )}`;
}

function getComment() {
  const text = commentInput.value;
  const name = userName.innerText;
  const role = userRole.innerText;
  const date = getDateString();

  return {
    text,
    name,
    role,
    date,
  };
}

function initRenderTasks(tasks, id) {
  if (!tasks) {
    console.log("нет задач в localStorage");
    return;
  }

  tasks.forEach((task) => {
    renderTaskAtDescription(task, id);
  });
}

function initCommentsRender(task) {
  if (!task.comments) {
    return;
  }
  task.comments.forEach((comment) => {
    renderComment(comment);
  });
}

let projects = getFromStorage("projects");
const currentProject = getFromStorage("currentProject");
let tasks = projects[currentProject];

header.innerText = currentProject;

let taskId = getIdFromUrl() || 1;

initRenderTasks(tasks, taskId);

const currentTask = tasks.find((task) => task.id === +taskId);

renderTaskDescription(currentTask);

initCommentsRender(currentTask);

submit.addEventListener("click", (e) => {
  e.preventDefault();

  const newComment = getComment();

  renderComment(newComment);

  currentTask.comments.push(newComment);

  projects[currentProject] = [
    ...projects[currentProject].filter((item) => item.id !== currentTask.id),
    currentTask,
  ];

  setToStorage("projects", projects);

  projects = getFromStorage("projects");
  tasks = projects[currentProject];
});

projectLinks.forEach((link) => {
  link.parentNode.addEventListener("click", (e) => {
    setToStorage("currentProject", link.parentNode.innerText);
  });
});
