import "../../../css/style.scss";
import { renderTask } from "./helpers";
import { constants } from "./constants";
import { initTasks, getFromStorage, setToStorage } from "../../../api";
import {addTeamCommands} from '../../team/scripts/index';
const { main, columns, addButtons, formTask, submit, projectLinks, header } =
  constants;

initTasks();
addTeamCommands();
let projects = getFromStorage("projects");
const currentProject = getFromStorage("currentProject");
let tasks = projects[currentProject];

header.innerText = currentProject;

function initRenderTasks(tasks) {
  if (!tasks) {
    console.log("нет задач в localStorage");
    return;
  }

  tasks.forEach((task) => {
    renderTask(task);
  });
}

initRenderTasks(tasks);

function toggleClasses(node, classes = []) {
  classes.forEach((el) => {
    node.classList.toggle(el);
  });
}

function toggleVisible(node) {
  node.style.display = node.style.display == "none" ? "" : "none";
}

function toggleView() {
  toggleClasses(constants.main, ["task__setup", "kanban__block", "flex"]);

  columns.forEach((column) => toggleVisible(column));
  toggleVisible(formTask);
}

function convertTag(tag) {
  const tagConverter = {
    маркетинг: "marketing",
    Бекенд: "development",
    Фронтенд: "development",
    Дизайн: "design",
  };

  return tagConverter[tag];
}

function getTask(targetButton, id) {
  const header = document.getElementById("taskTitle").value;

  const tagDropdown = document.querySelector(".tag__dropdown");
  const tag = tagDropdown.querySelector(".label").textContent;
  const convertedTag = convertTag(tag);

  const description = document.getElementById("taskDescription").value;
  const deadline = document.getElementById("deadline").value;
  const executor = document.getElementById("executor").value;

  const targetColumn = targetButton.closest(".column.task__column");
  const stage = targetColumn.querySelector(".item__title").innerText;

  return {
    id,
    header,
    tag: convertedTag,
    description,
    deadline,
    executor,
    stage,
    comments: [],
  };
}

let targetButton;

addButtons.forEach((button) => {
  button.addEventListener("click", (e) => {
    e.preventDefault();

    toggleView();

    targetButton = e.target;
  });
});

submit.addEventListener("click", (e) => {
  e.preventDefault();

  toggleView();

  const id = tasks.length + 1;

  const task = getTask(targetButton, id);

  renderTask(task);

  projects[currentProject] = [...projects[currentProject], task];

  setToStorage("projects", projects);

  projects = getFromStorage("projects");
  tasks = projects[currentProject];

  targetButton = null;
});

projectLinks.forEach((link) => {
  link.parentNode.addEventListener("click", (e) => {
    setToStorage("currentProject", link.parentNode.innerText);
  });
});
