export const constants = {
  main: document.querySelector("main"),
  columns: document.querySelectorAll(".task__column"),
  addButtons: document.querySelectorAll(".item__btn"),
  formTask: document.querySelector(".form--task"),
  submit: document.querySelector('input[type="submit"]'),
  projectLinks: document.querySelectorAll('.projects__icon'),
  header: document.querySelector('h1')
};