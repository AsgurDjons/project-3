export function createTask(task) {
  return `<a href="task-description.html?id=${
    task.id
  }" class="task task__block">
<p class="task__description">${task.header}</p>
<div class="flex">
  <img src="img/userpic.jpg" class="task__executor" alt="">
  <p class="task__label ${task.tag}">${task.tag.toUpperCase()}</p>
</div>
${task.image ? `<img src= ${task.image} class="task__img" alt=""></img>` : ""}
</a>`;
}

export function renderTask(task) {
  const taskHTML = createTask(task);

  const targetHeader = Array.from(
    document.querySelectorAll(".item__title")
  ).find((el) => el.textContent === task.stage);

  const targetColumn = targetHeader.closest(".column.task__column");

  targetColumn.insertAdjacentHTML("beforeend", taskHTML);
}
