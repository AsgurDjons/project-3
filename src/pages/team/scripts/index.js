import '../../../css/style.scss';
const users = JSON.parse(localStorage.getItem('credentuals'));

const team = document.querySelector('.team');
const teams = ['Фронтенд', 'Бекенд', 'Финансы', 'Маркетинг', 'Продажи', 'Дизайн'];


export function addTeamCommands() {
    let out ='';
    let count = 0;
    teams.forEach(item => {
        users.forEach(elem => {
            if (elem.team === item) {
                count++;
                out += `
                <div class="team__description flex">
                    <p class="team__side">${elem.team}</p>
                    <div class="team__members flex">
                        ${'<img src="img/people/Oval-1.jpg" alt="">'.repeat(count)}
                    </div>
                </div>
                `;
            }
        });
        count = 0;
    });
    console.log(out);
    team.innerHTML = out;
}
addTeamCommands();