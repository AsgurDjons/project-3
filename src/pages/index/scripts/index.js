import '../../../css/style.scss';
import { register, logins, submitReg, formReg, loginOut,
     elementsBlock, loginUp, goToMain, reg, commandsAll} from './variableDOM';

const objForm = {
    email: null,
    pass: null,
    user: null,
};
let user;
const credentuals = (localStorage.getItem('credentuals') !== null)? JSON.parse(localStorage.getItem('credentuals')): [];
hidden(elementsBlock);
show(elementsBlock, 1);


// Task-1 Form
// 1 валидация формы 
function emailValid(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function form(register, objForm, credentuals) {
    const {email, pass, user} = register;
    const out = user.value.split(' ').filter(item => item);
    formReset(objForm);
    console.log(emailValid(email.value));
    if (emailValid(email.value) === true && checkLogin(credentuals, email.value) === false) {
        objForm.email = email.value;
    }
    if (pass.value.length >= 8) {
        objForm.pass = pass.value;
    }
    //  я нарушил условия тз с валидацией имени и фамилии из 3 символов на 2 
    // потому что имя такие как Ян или фамилия Ли не смогут зарегистрироваться!
    if (out.length === 2 && out[0].length >= 2 && out[1].length >= 2) {
        let name = '';
        out.forEach(item => {
            name +=`${item[0].toUpperCase()}` + `${item.slice(1, item.length)} `;
        });
        objForm.user = name.trim();
    }
}

// проверка на совпадения email
function checkLogin(arr, email) {
    return arr.some(item => item.email === email);
}
///

// аннулирование объекта
function formReset(obj) {
    for (const key in obj) {
        obj[key] = null;
    }
}
//////

// 
function saveLocal(array) {
    localStorage.setItem('credentuals', JSON.stringify(array));
}

// отправка формы в локальное хранилище (сервер)
submitReg.addEventListener('click', (e) => {
    e.preventDefault();
    form(register, objForm, credentuals);
    let valid = false;
    for (const key in objForm) {
        if (objForm[key] !== null) {
            valid = true;
        }else {
            valid = false;
            break;
        }
    }
    if (valid === true) {
        credentuals.push(objForm);
        saveLocal(credentuals);
        formReg.reset();
        formReset(objForm);
    }
});
/////

////// Скрывает блоки страницы
function hidden(elementsBlock) {
    elementsBlock.forEach(elem => {
        elem.style.display = 'none';
    });
}

///// Показывает блоки страницы
function show(elementsBlock, ind = 0) {
    elementsBlock[ind].style.display = 'block';
}

// Переключает на блок логина 
loginOut.addEventListener('click', () => {
    hidden(elementsBlock);
    show(elementsBlock, 1);
});
//////////

// reg
reg.addEventListener('click', () => {
    hidden(elementsBlock);
    show(elementsBlock, 0);
});

////// проверка логина и пароля
function login(logins) {
    const {email, pass} = logins;
    let valid = emailValid(email.value)? true: false;
    let out;
    if (valid) {
        out = credentuals.find(item => item.email === email.value);
    }
    if (valid && out?.email === email.value && out?.pass === pass.value) {
        user = email.value;
        hidden(elementsBlock);
        show(elementsBlock, 2);
    } else {
        alert('не правильные данные!');
    }
}

// сабмит логина
 loginUp.addEventListener('click', (e) => {
    e.preventDefault();
    login(logins);
 });

//  переход на страницу !
goToMain.addEventListener('click', (e) => {
    e.preventDefault();
    commands();
    window.location.href = "task-board.html";
});

function commands() {
    const userArr = JSON.parse(localStorage.getItem('credentuals'));
    const editableUser =  userArr.find(item => item.email === user);
    const newUserArray = userArr.filter(item => item.email !== user);
    commandsAll.forEach(item => {
        if (item.children[0].checked) {
            editableUser.team = item.innerText;
        }
    });
    newUserArray.push(editableUser);
    saveLocal(newUserArray);
}
