// логика работы с локальным хранилищем
export function initTasks() {
  const tasks = [
    {
      id: 1,
      header: "Find top 5 customer requests",
      stage: "Беклог",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "marketing",
      image: null,
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
      finished: true,
    },
    {
      id: 2,
      header: "E-mail after registration so that I can confirm my",
      stage: "Беклог",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "development",
      image: null,
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
    {
      id: 3,
      header: "Two-factor authentication to make private",
      stage: "Беклог",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "design",
      image: "img/img_prev.jpg",
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
    {
      id: 4,
      header: "Two-factor authentication to make private",
      stage: "Выполнить",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "design",
      image: null,
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
    {
      id: 5,
      header: "Find top 5 customer requests",
      stage: "Выполнить",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "development",
      image: "img/img_prev.jpg",
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
    {
      id: 6,
      header: "E-mail after registration so that I can confirm my",
      stage: "Выполнить",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "development",
      image: "img/img_prev.jpg",
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
    {
      id: 7,
      header: "E-mail after registration so that I can confirm my",
      stage: "Выполнено",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "development",
      image: "img/img_prev.jpg",
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
    {
      id: 8,
      header: "E-mail after registration so that I can confirm my",
      stage: "Выполнено",
      executor: "Linzell Bowman",
      deadline: "Tue, Dec 25",
      tag: "marketing",
      image: null,
      description:
        "Описания задач используются при планировании проектов, исполнении проектов и контроле за ними. При планировании проекта описания задач используются для планирования объема работ и составления смет. Во время выполнения проекта описание задач используется теми, кто выполняет работы, чтобы обеспечить их правильное выполнение.",
      comments: [
        {
          name: "Елена Стрельцова",
          role: "дизайнер",
          text: "During a project build, it is necessary to evaluate the product design and development against project requirements and outcomes",
          date: "Вчера, 12:37",
        },
        {
          name: "Prescott MacCaffery",
          role: "Developer",
          text: '<a href="#" class="person__tag">@Helena</a> Software quality assurance activity in which one or several humans check a program mainly',
          date: "Вчера, 12:37",
        },
      ],
    },
  ];

  const projects = {
    'Запуск приложения': tasks,
    'Редизайн сайта': tasks,
    'Чёрная пятница': tasks,
    'Проект №07': tasks,
  };

  if (getFromStorage("projects")) {
    return;
  }

  setToStorage('projects', projects);
  setToStorage('currentProject', 'Редизайн сайта');
}

export function getFromStorage(key) {
  return JSON.parse(localStorage.getItem(key));
}

export function setToStorage(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}

export function updateArrayItem(key, oldValue, updatedItem) {
  const newValue = [
    ...oldValue.filter((item) => item.id !== updatedItem.id),
    updatedItem,
  ];

  setToStorage(key, newValue);
}
